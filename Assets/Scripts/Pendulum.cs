using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{	

	[SerializeField]
	public GameObject pendulum;
	[SerializeField]
	public GameObject roofHandle;
	[SerializeField]
	public GameObject paint;
	[SerializeField]
	public float length;
	[SerializeField]
	public float dragCoefficient = 0.03f;
	[SerializeField]
	public float mass = 1.0f;
	[SerializeField]
	public float scale = 1.0f;
	int cnt =0;
	private float gravity = 9.81f;
	private float[] angles = {0.0f , 0.20f + 2.0f, Mathf.PI/2.0f  , 0.0f};


	float[] sumArrays(float[] x , float[] y) {

		float[] labels = new float[x.Length];

		for(int i = 0; i < x.Length ; i++) {
			labels[i] = x[i] + y[i];
		}

		return labels;
	}

	float[] mulArrays(float[] x , float[] y) {

		float[] labels = new float[x.Length];

		for(int i = 0; i < x.Length ; i++) {
			labels[i] = x[i] * y[i];
		}

		return labels;
	}

	float[] divideArrays(float[] x , float[] y) {

		float[] labels = new float[x.Length];

		for(int i = 0; i < x.Length ; i++) {
			labels[i] = x[i] / y[i];
		}

		return labels;
	}

	float[] sumArrayWithNumber(float[] x , float y) {

		float[] labels = new float[x.Length];

		for(int i = 0; i < x.Length ; i++) {
			labels[i] = x[i] + y;
		}

		return labels;
	}

	float[] mulArrayWithNumber(float[] x , float y) {

		float[] labels = new float[x.Length];

		for(int i = 0; i < x.Length ; i++) {
			labels[i] = x[i] * y;
		}

		return labels;
	}

	float[] divideArrayWithNumber(float[] x , float y) {

		float[] labels = new float[x.Length];

		for(int i = 0; i < x.Length ; i++) {
			labels[i] = x[i] / y;
		}

		return labels;
	}

	float[] G(float[] y) {

		float θ_dd , Φ_dd , θ_d , Φ_d , θ , Φ;

		θ = y[2];
		Φ = y[3];

		θ_d = y[0];
		Φ_d = y[1];


		θ_dd = Mathf.Pow(Φ_d , 2)* Mathf.Cos(θ) * Mathf.Sin(θ) - gravity/length * Mathf.Sin(θ) - (dragCoefficient * θ_d/mass);
		Φ_dd = -2.0f * θ_d * Φ_d / Mathf.Tan(θ) -  (dragCoefficient * Φ_d/mass);

		return new[] {θ_dd , Φ_dd , θ_d , Φ_d};

	}

	float[] RK4_step(float[] angles) {

		float dt = Time.deltaTime;

		float[] k1 = G(angles);

		float[] k2 = G(sumArrays(angles , mulArrayWithNumber(mulArrayWithNumber(k1 , 0.5f) , dt)));

		float[] k3 = G(sumArrays(angles , mulArrayWithNumber(mulArrayWithNumber(k2 , 0.5f) , dt)));

		float[] k4 = G(sumArrays(angles , mulArrayWithNumber(k3 , dt)));

		return mulArrayWithNumber(divideArrayWithNumber(sumArrays(sumArrays(sumArrays(k1 , mulArrayWithNumber(k2 , 2.0f)) , mulArrayWithNumber(k3 , 2.0f)) , k4) , 6.0f) , dt);
	}

	void Start()
	{
		length = Vector3.Distance (pendulum.transform.position, roofHandle.transform.position);
	}

	void Update() 
	{	
		pendulum.transform.rotation = Quaternion.FromToRotation(-Vector3.up , pendulum.transform.position - new Vector3(0 , length , 0));
	}

	void FixedUpdate()
	{	
		if(cnt % 2 == 0)	
		Instantiate(paint , new Vector3(pendulum.transform.position.x , -4.5f , pendulum.transform.position.z) , Quaternion.identity);	

		cnt++;
		float θ = angles[2];
		float ϕ = angles[3];

		pendulum.transform.position = new Vector3(scale*length * Mathf.Sin(θ) * Mathf.Cos(ϕ)
										 	, -scale*length * Mathf.Cos(θ)
										  	, scale*length * Mathf.Sin(θ) * Mathf.Sin(ϕ));

		
	    angles = sumArrays(angles , RK4_step(angles));

	}
}
