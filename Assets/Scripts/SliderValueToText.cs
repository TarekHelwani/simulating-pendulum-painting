using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SliderValueToText : MonoBehaviour
{   
    private TMP_Text textSliderValue;

    void Start (){
        textSliderValue = GetComponent<TMP_Text>();
    }


    public void showValue(float value) {
        textSliderValue.text = "" + value;
    }
}

