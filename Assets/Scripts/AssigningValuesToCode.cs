using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssigningValuesToCode : MonoBehaviour
{
    public void onSlideChange(float value) {
        PlayerPrefs.SetFloat(gameObject.tag, value);
    }
}
